package com.example.agenda;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListaActivity extends AppCompatActivity {

    private TableLayout tblLista;
    public static ArrayList<Contacto> contactos;
    private Button btnNuevo;
    ArrayList<Contacto> filter;
    private int indexActual;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        TableRow row = new TableRow(ListaActivity.this);

        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        filter = contactos;
        indexActual = (int) bundleObject.getInt("indexActual");
        this.filter = this.contactos;
        cargarContactos();
        btnNuevo = (Button) findViewById(R.id.btnNuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                Bundle oBundle = new Bundle();
                oBundle.putSerializable("contactosSaved", contactos);
                oBundle.putInt("indexActual", indexActual);
                oBundle.putBoolean("nuevo", true);
                i.putExtras(oBundle);
                setResult(RESULT_OK,i);
                finish();

            }
        });
    }

    public void cargarContactos() {
        for(int x=0; x < contactos.size(); x++) {
            final Contacto c = contactos.get(x);
            final long index = contactos.get(x).get_ID();
            TableRow nRow = new TableRow(ListaActivity.this);

            TextView nText = new TextView(ListaActivity.this);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorite()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListaActivity.this);
            nButton.setText(R.string.accion_ver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);
            Button btnBorrar = new Button (ListaActivity.this);
            btnBorrar.setText(R.string.btnBorrar);
            btnBorrar.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            btnBorrar.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c= (Contacto) v.getTag(R.string.contacto_g);

                    Intent i = new Intent ();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contactos", c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    oBundle.putSerializable("contactosSaved", contactos);

                    oBundle.putInt("indexActual", indexActual);
                    oBundle.putBoolean("nuevo", false);

                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int y = 0; y < filter.size(); y++){
                        if(filter.get(y).get_ID() == (int) c.get_ID()){
                            filter.remove(filter.get(y));
                            contactos = filter;
                        }
                    }
                    quitarElemento();

                    Toast.makeText(ListaActivity.this, R.string.mensajeBorrar, Toast.LENGTH_SHORT).show();
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            btnBorrar.setTag(R.string.contacto_g, c);
            btnBorrar.setTag(R.string.contacto_g_index,x);
            nRow.addView(btnBorrar);
            tblLista.addView(nRow);
        }
    }

    private void quitarElemento() {
        tblLista.removeAllViews();

        TableRow headRow = new TableRow(ListaActivity.this);
        TextView nombre = new TextView(ListaActivity.this);
        nombre.setText(R.string.nombre);
        nombre.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        TextView accion = new TextView(ListaActivity.this);

        accion.setText(R.string.accion);
        accion.setTextSize(TypedValue.COMPLEX_UNIT_PT,8);
        headRow.addView(nombre);
        headRow.addView(accion);
        tblLista.addView(headRow);

        cargarContactos();
    }

    public void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).getNombre().contains(s))
                list.add(filter.get(x));
        }

        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview ,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu); }
}
